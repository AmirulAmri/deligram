/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/assets/css/style.css":
/*!**********************************!*\
  !*** ./src/assets/css/style.css ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack://deligram/./src/assets/css/style.css?");

/***/ }),

/***/ "./src/assets/js/app.js":
/*!******************************!*\
  !*** ./src/assets/js/app.js ***!
  \******************************/
/***/ (() => {

eval("feather.replace();\r\n\r\n$(document).ready(function() {\r\n    $(\"input#intlTelPhone\").intlTelInput({\r\n        initialCountry: \"my\",\r\n        onlyCountries: [\"my\"],\r\n        allowDropdown: false,\r\n        utilsScript: \"https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/js/utils.js\",\r\n    });\r\n});\n\n//# sourceURL=webpack://deligram/./src/assets/js/app.js?");

/***/ }),

/***/ "./src/components.js":
/*!***************************!*\
  !*** ./src/components.js ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _assets_images_logo_blend_png__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./assets/images/logo-blend.png */ \"./src/assets/images/logo-blend.png\");\n/* harmony import */ var _assets_images_shipping_png__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./assets/images/shipping.png */ \"./src/assets/images/shipping.png\");\n/* harmony import */ var _assets_images_product_blend_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./assets/images/product-blend.png */ \"./src/assets/images/product-blend.png\");\n/* harmony import */ var _assets_images_fpx_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./assets/images/fpx.png */ \"./src/assets/images/fpx.png\");\n/* harmony import */ var _assets_images_banner_blend_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./assets/images/banner-blend.png */ \"./src/assets/images/banner-blend.png\");\n/* harmony import */ var _assets_images_bank_affin_bank_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./assets/images/bank/affin-bank.png */ \"./src/assets/images/bank/affin-bank.png\");\n/* harmony import */ var _assets_images_bank_alliance_bank_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./assets/images/bank/alliance-bank.png */ \"./src/assets/images/bank/alliance-bank.png\");\n/* harmony import */ var _assets_images_bank_ambank_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./assets/images/bank/ambank.png */ \"./src/assets/images/bank/ambank.png\");\n/* harmony import */ var _assets_images_bank_bank_islam_png__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./assets/images/bank/bank-islam.png */ \"./src/assets/images/bank/bank-islam.png\");\n/* harmony import */ var _assets_images_bank_bank_muamalat_png__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./assets/images/bank/bank-muamalat.png */ \"./src/assets/images/bank/bank-muamalat.png\");\n/* harmony import */ var _assets_images_bank_bank_rakyat_png__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./assets/images/bank/bank-rakyat.png */ \"./src/assets/images/bank/bank-rakyat.png\");\n/* harmony import */ var _assets_images_bank_bsn_png__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./assets/images/bank/bsn.png */ \"./src/assets/images/bank/bsn.png\");\n/* harmony import */ var _assets_images_bank_cimb_png__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./assets/images/bank/cimb.png */ \"./src/assets/images/bank/cimb.png\");\n/* harmony import */ var _assets_images_bank_hong_leong_bank_png__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./assets/images/bank/hong-leong-bank.png */ \"./src/assets/images/bank/hong-leong-bank.png\");\n/* harmony import */ var _assets_images_bank_hsbc_bank_png__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./assets/images/bank/hsbc-bank.png */ \"./src/assets/images/bank/hsbc-bank.png\");\n/* harmony import */ var _assets_images_bank_kfh_bank_png__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./assets/images/bank/kfh-bank.png */ \"./src/assets/images/bank/kfh-bank.png\");\n/* harmony import */ var _assets_images_bank_m2u_png__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./assets/images/bank/m2u.png */ \"./src/assets/images/bank/m2u.png\");\n/* harmony import */ var _assets_images_bank_ocbc_bank_png__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./assets/images/bank/ocbc-bank.png */ \"./src/assets/images/bank/ocbc-bank.png\");\n/* harmony import */ var _assets_images_bank_public_bank_png__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./assets/images/bank/public-bank.png */ \"./src/assets/images/bank/public-bank.png\");\n/* harmony import */ var _assets_images_bank_rhb_png__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./assets/images/bank/rhb.png */ \"./src/assets/images/bank/rhb.png\");\n/* harmony import */ var _assets_images_bank_standard_chartered_png__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./assets/images/bank/standard-chartered.png */ \"./src/assets/images/bank/standard-chartered.png\");\n/* harmony import */ var _assets_images_bank_uob_png__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./assets/images/bank/uob.png */ \"./src/assets/images/bank/uob.png\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\n\n//# sourceURL=webpack://deligram/./src/components.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _components_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components.js */ \"./src/components.js\");\n/* harmony import */ var _assets_css_style_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./assets/css/style.css */ \"./src/assets/css/style.css\");\n/* harmony import */ var _assets_js_app_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./assets/js/app.js */ \"./src/assets/js/app.js\");\n/* harmony import */ var _assets_js_app_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_assets_js_app_js__WEBPACK_IMPORTED_MODULE_2__);\n\r\n\r\n\n\n//# sourceURL=webpack://deligram/./src/index.js?");

/***/ }),

/***/ "./src/assets/images/bank/affin-bank.png":
/*!***********************************************!*\
  !*** ./src/assets/images/bank/affin-bank.png ***!
  \***********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/affin-bank.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/affin-bank.png?");

/***/ }),

/***/ "./src/assets/images/bank/alliance-bank.png":
/*!**************************************************!*\
  !*** ./src/assets/images/bank/alliance-bank.png ***!
  \**************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/alliance-bank.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/alliance-bank.png?");

/***/ }),

/***/ "./src/assets/images/bank/ambank.png":
/*!*******************************************!*\
  !*** ./src/assets/images/bank/ambank.png ***!
  \*******************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/ambank.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/ambank.png?");

/***/ }),

/***/ "./src/assets/images/bank/bank-islam.png":
/*!***********************************************!*\
  !*** ./src/assets/images/bank/bank-islam.png ***!
  \***********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/bank-islam.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/bank-islam.png?");

/***/ }),

/***/ "./src/assets/images/bank/bank-muamalat.png":
/*!**************************************************!*\
  !*** ./src/assets/images/bank/bank-muamalat.png ***!
  \**************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/bank-muamalat.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/bank-muamalat.png?");

/***/ }),

/***/ "./src/assets/images/bank/bank-rakyat.png":
/*!************************************************!*\
  !*** ./src/assets/images/bank/bank-rakyat.png ***!
  \************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/bank-rakyat.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/bank-rakyat.png?");

/***/ }),

/***/ "./src/assets/images/bank/bsn.png":
/*!****************************************!*\
  !*** ./src/assets/images/bank/bsn.png ***!
  \****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/bsn.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/bsn.png?");

/***/ }),

/***/ "./src/assets/images/bank/cimb.png":
/*!*****************************************!*\
  !*** ./src/assets/images/bank/cimb.png ***!
  \*****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/cimb.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/cimb.png?");

/***/ }),

/***/ "./src/assets/images/bank/hong-leong-bank.png":
/*!****************************************************!*\
  !*** ./src/assets/images/bank/hong-leong-bank.png ***!
  \****************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/hong-leong-bank.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/hong-leong-bank.png?");

/***/ }),

/***/ "./src/assets/images/bank/hsbc-bank.png":
/*!**********************************************!*\
  !*** ./src/assets/images/bank/hsbc-bank.png ***!
  \**********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/hsbc-bank.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/hsbc-bank.png?");

/***/ }),

/***/ "./src/assets/images/bank/kfh-bank.png":
/*!*********************************************!*\
  !*** ./src/assets/images/bank/kfh-bank.png ***!
  \*********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/kfh-bank.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/kfh-bank.png?");

/***/ }),

/***/ "./src/assets/images/bank/m2u.png":
/*!****************************************!*\
  !*** ./src/assets/images/bank/m2u.png ***!
  \****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/m2u.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/m2u.png?");

/***/ }),

/***/ "./src/assets/images/bank/ocbc-bank.png":
/*!**********************************************!*\
  !*** ./src/assets/images/bank/ocbc-bank.png ***!
  \**********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/ocbc-bank.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/ocbc-bank.png?");

/***/ }),

/***/ "./src/assets/images/bank/public-bank.png":
/*!************************************************!*\
  !*** ./src/assets/images/bank/public-bank.png ***!
  \************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/public-bank.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/public-bank.png?");

/***/ }),

/***/ "./src/assets/images/bank/rhb.png":
/*!****************************************!*\
  !*** ./src/assets/images/bank/rhb.png ***!
  \****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/rhb.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/rhb.png?");

/***/ }),

/***/ "./src/assets/images/bank/standard-chartered.png":
/*!*******************************************************!*\
  !*** ./src/assets/images/bank/standard-chartered.png ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/standard-chartered.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/standard-chartered.png?");

/***/ }),

/***/ "./src/assets/images/bank/uob.png":
/*!****************************************!*\
  !*** ./src/assets/images/bank/uob.png ***!
  \****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/uob.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/bank/uob.png?");

/***/ }),

/***/ "./src/assets/images/banner-blend.png":
/*!********************************************!*\
  !*** ./src/assets/images/banner-blend.png ***!
  \********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/banner-blend.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/banner-blend.png?");

/***/ }),

/***/ "./src/assets/images/fpx.png":
/*!***********************************!*\
  !*** ./src/assets/images/fpx.png ***!
  \***********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/fpx.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/fpx.png?");

/***/ }),

/***/ "./src/assets/images/logo-blend.png":
/*!******************************************!*\
  !*** ./src/assets/images/logo-blend.png ***!
  \******************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/logo-blend.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/logo-blend.png?");

/***/ }),

/***/ "./src/assets/images/product-blend.png":
/*!*********************************************!*\
  !*** ./src/assets/images/product-blend.png ***!
  \*********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/product-blend.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/product-blend.png?");

/***/ }),

/***/ "./src/assets/images/shipping.png":
/*!****************************************!*\
  !*** ./src/assets/images/shipping.png ***!
  \****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"images/shipping.png\";\n\n//# sourceURL=webpack://deligram/./src/assets/images/shipping.png?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		__webpack_require__.p = "./";
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.js");
/******/ 	
/******/ })()
;