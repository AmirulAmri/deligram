const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

module.exports = {
    entry: {
        main: path.resolve(__dirname, './src/index.js')
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: './',
        assetModuleFilename: 'images/[name][ext][query]',
    },
    devServer: {
        static: './dist',
        open: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Deligram',
            filename: 'index.html',
            template: path.resolve(__dirname, './src/layout/main-layout.html'),
            inject: false
        }),
        new HtmlWebpackPlugin({
            title: 'Deligram',
            filename: 'added-to-cart.html',
            template: path.resolve(__dirname, './src/layout/main-layout.html'),
            inject: false
        }),
        new HtmlWebpackPlugin({
            title: 'Deligram',
            filename: 'cart.html',
            template: path.resolve(__dirname, './src/layout/main-layout.html'),
            inject: false
        }),
        new HtmlWebpackPlugin({
            title: 'Deligram',
            filename: 'checkout-1.html',
            template: path.resolve(__dirname, './src/layout/main-layout.html'),
            inject: false
        }),
        new HtmlWebpackPlugin({
            title: 'Deligram',
            filename: 'checkout-2.html',
            template: path.resolve(__dirname, './src/layout/main-layout.html'),
            inject: false
        }),
        new HtmlWebpackPlugin({
            title: 'Deligram',
            filename: 'desktop-view-2.html',
            template: path.resolve(__dirname, './src/layout/main-layout.html'),
            inject: false
        }),
        new HtmlWebpackPlugin({
            title: 'Deligram',
            filename: 'thank-you.html',
            template: path.resolve(__dirname, './src/layout/main-layout.html'),
            inject: false
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css'
        })
    ],
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            },
            {
                test: /\.(ico|gif|png|jpg|jpeg|svg)$/i,
                type: 'asset/resource',
            },
        ],
    },
}